package matias.easygps;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.net.URISyntaxException;
import java.util.Dictionary;
import java.util.Hashtable;

import matias.easygps.Services.Constants;
import matias.easygps.Services.VolleySingleton;

public class RealTimeTrack extends AppCompatActivity  {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Socket socket;
    private Marker marker;
    private MarkerOptions markerOpt;
    private Dictionary<String,Marker> markers;

    {
        try {
            socket = IO.socket(Constants.CHAT_SERVER_URL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.turnOff) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.CHAT_SERVER_URL + "/messagetotracker?imei=011691003111617&command=C01&param1=true&param2=false&param3=false",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i("Respuesta", response.toString());
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Handle error
                            Log.i("Respuesta", error.toString());
                        }
                    });

            VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
            /*Intent intent = new Intent(this, mydivices.class);
            startActivity(intent);*/
        }
        else if(id == R.id.turnOn){
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.CHAT_SERVER_URL + "/messagetotracker?imei=011691003111617&command=C01&param1=false&param2=false&param3=false",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i("Respuesta", response.toString());
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Handle error
                            Log.i("Respuesta", error.toString());
                        }
                    });

            VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        markers = new Hashtable<>();
        socket.on("move", onMove);
        socket.connect();
        Log.i("info", "Pasa aca");
        setContentView(R.layout.activity_real_time_track);
        //setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    turnOnDivice();
                }
            });

            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                //setUpMap();
            }
        }
    }

    private void turnOnDivice(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.CHAT_SERVER_URL + "/messagetotracker?imei=011691003111617&command=C01&param1=true&param2=false&param3=false",
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("Respuesta", response.toString());
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // Handle error
                    Log.i("Respuesta", error.toString());
                }
            });
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private Emitter.Listener onMove = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
           runOnUiThread(new Runnable() {
               @Override
               public void run() {
                   JSONObject data = (JSONObject) args[0];
                   Log.v("data", data.toString());
                   try {
                       if (!markers.isEmpty() && markers.get(data.getString("imei")) != null) {
                           Marker mkr = markers.get(data.getString("imei"));
                           System.out.println("Coordenadas marker: " + mkr.getPosition().toString());
                           mkr.setPosition(new LatLng(data.getDouble("lat"), data.getDouble("lng")));
                           mkr.setSnippet("Imei: " + data.getString("imei") + "\n Gps Time: " + data.getString("gpsTime"));

                       } else {
                           markerOpt = new MarkerOptions().position(new LatLng(data.getDouble("lat"), data.getDouble("lng"))).title("Moto");
                           marker = mMap.addMarker(new MarkerOptions().position(new LatLng(data.getDouble("lat"), data.getDouble("lng"))).title("Moto"));
                           marker.setSnippet("Imei: " + data.getString("imei") + "\n Gps Time: " + data.getString("gpsTime"));
                           markers.put(data.getString("imei"),marker);
                       }

                   } catch (JSONException jsE) {
                       Log.d("Error", jsE.getMessage());
                   } catch (Exception e) {
                       Log.d("Error", e.getMessage());
                   }

                   // add the message to view
               }
           });
        }
    };


   /* private void setUpMap() {
        markerOpt = new MarkerOptions().position(new LatLng(-32.313475, -58.057440)).title("Moto");
        mMap.addMarker(marker);

    }
*/

}
