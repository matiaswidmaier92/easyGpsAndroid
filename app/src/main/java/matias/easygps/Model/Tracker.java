package matias.easygps.Model;

import java.util.Date;

public class Tracker {
    private Integer id;
    private String imei;
    private String descripcion;
    private Date gpsTime;


    public Tracker(Integer id, String imei, String descripcion, Date gpsTime){
        this.imei=imei;
        this.descripcion=descripcion;
        this.gpsTime=gpsTime;
    }

    public Integer getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Date getGpsTime() {
        return gpsTime;
    }

    public String getImei() {
        return imei;
    }
}
